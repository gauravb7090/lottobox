package etc;

import com.google.gson.Gson;

import javax.inject.Singleton;

@Singleton
public class JsonConverter {

    public static Gson gson = new Gson();

    public static String to(Object o) {
        return gson.toJson(o);
    }

    public static Object from(String str, Class clazz) {
        return gson.fromJson(str, clazz);
    }
}
