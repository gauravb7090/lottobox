package etc;

import com.google.inject.Inject;
import ninja.*;
import org.slf4j.Logger;

public class JsonFilter implements Filter {

    private final Ninja ninja;
    private final Logger logger;

    @Inject
    public JsonFilter(Ninja ninja, Logger logger) {
        this.ninja = ninja;
        this.logger = logger;
    }

    @Override
    public Result filter(FilterChain chain, Context context) {
        Result result = chain.next(context);
        return result
                .supportedContentTypes(Result.APPLICATION_JSON)
                .fallbackContentType(Result.APPLICATION_JSON);
    }

    private Result forbidden(Context context) {
        return ninja.getForbiddenResult(context);
    }
}