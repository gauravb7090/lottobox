package etc;

import com.google.inject.Inject;
import handlers.GameHandler;
import models.dto.GameDto;
import ninja.*;
import org.slf4j.Logger;

import java.util.Optional;

public class CurrentGameFilter implements Filter {

    private final Ninja ninja;
    private final GameHandler gameHandler;
    private final Logger logger;

    @Inject
    public CurrentGameFilter(Ninja ninja, GameHandler gameHandler, Logger logger) {
        this.ninja = ninja;
        this.gameHandler = gameHandler;
        this.logger = logger;
    }

    @Override
    public Result filter(FilterChain chain, Context context) {
        final Optional<GameDto> gameByCookie = gameHandler.getGameByCookie(context);
        if (gameByCookie.isPresent()) {
            context.setAttribute("game", gameByCookie.get());
            return chain.next(context);
        }
        return forbidden(context);
    }

    private Result forbidden(Context context) {
        return ninja.getForbiddenResult(context);
    }
}