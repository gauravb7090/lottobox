package etc;

import handlers.GameHandler;
import models.dto.GameDto;
import ninja.Context;
import ninja.params.ArgumentExtractor;
import org.slf4j.Logger;

import javax.inject.Inject;

public class CurrentGameExtractor implements ArgumentExtractor<GameDto> {

    private final GameHandler gameHandler;
    private final Logger logger;

    @Inject
    public CurrentGameExtractor(GameHandler gameHandler, Logger logger) {
        this.gameHandler = gameHandler;
        this.logger = logger;
    }

    @Override
    public GameDto extract(Context context) {
        return context.getAttribute("game", GameDto.class);
    }

    @Override
    public Class<GameDto> getExtractedType() {
        return GameDto.class;
    }

    @Override
    public String getFieldName() {
        return null;
    }

}
