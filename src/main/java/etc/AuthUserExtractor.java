package etc;

import handlers.UserHandler;
import models.dto.UserDto;
import ninja.Context;
import ninja.params.ArgumentExtractor;
import org.slf4j.Logger;

import javax.inject.Inject;

public class AuthUserExtractor implements ArgumentExtractor<UserDto> {

    private final UserHandler userHandler;
    private final Logger logger;

    @Inject
    public AuthUserExtractor(UserHandler userHandler, Logger logger) {
        this.userHandler = userHandler;
        this.logger = logger;
    }

    @Override
    public UserDto extract(Context context) {
        return context.getAttribute("user", UserDto.class);
    }

    @Override
    public Class<UserDto> getExtractedType() {
        return UserDto.class;
    }

    @Override
    public String getFieldName() {
        return null;
    }

}
