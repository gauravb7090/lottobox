/**
 * Copyright (C) 2012-2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package etc;

import com.google.inject.Inject;
import handlers.UserHandler;
import models.dto.UserDto;
import ninja.*;
import org.slf4j.Logger;

import java.util.Optional;

public class AuthFilter implements Filter {

    private final Ninja ninja;
    private final UserHandler userHandler;
    private final Logger logger;

    @Inject
    public AuthFilter(Ninja ninja, UserHandler userHandler, Logger logger) {
        this.ninja = ninja;
        this.userHandler = userHandler;
        this.logger = logger;
    }

    @Override
    public Result filter(FilterChain chain, Context context) {
        final Optional<UserDto> userByCookie = userHandler.getUserByCookie(context);
        if (userByCookie.isPresent()) {
            context.setAttribute("user", userByCookie.get());
            return chain.next(context);
        }
        return forbidden(context);
    }

    private Result forbidden(Context context) {
        return ninja.getForbiddenResult(context);
    }
}