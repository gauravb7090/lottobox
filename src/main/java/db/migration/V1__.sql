create table claims (
    claim_id bytea not null,
    createdAt timestamp not null,
    updatedAt timestamp not null,
    type int4 not null,
    game_id bytea not null,
    user_id bytea not null,
    ticket_id bytea not null,
    primary key (claim_id)
);

create table games (
    game_id bytea not null,
    createdAt timestamp not null,
    updatedAt timestamp not null,
    claiming_window int8 not null,
    game_code varchar(255) not null,
    col int4 not null,
    generationWindow int8 not null,
    numberGeneratedAt timestamp not null,
    numberIdx int4 not null,
    numbers oid,
    reconnect_threshold int8 not null,
    row int4 not null,
    status int4 not null,
    user_id bytea not null,
    primary key (game_id)
);

create table games_users (
    game_id bytea not null,
    user_id bytea not null,
    primary key (game_id, user_id)
);

create table tickets (
    ticket_id bytea not null,
    createdAt timestamp not null,
    updatedAt timestamp not null,
    ticket_matrix bytea not null,
    user_id bytea not null,
    game_id bytea not null,
    primary key (ticket_id)
);

create table users (
    user_id bytea not null,
    createdAt timestamp not null,
    updatedAt timestamp not null,
    name varchar(255) not null,
    primary key (user_id)
);

alter table games
add constraint UK_8hdjosjf54ji4q9ihssgf7l6  unique (game_code);

alter table claims
add constraint FK_k6q2m8mynvgxl5grtdl0n9b6g
foreign key (game_id)
references games;

alter table claims
add constraint FK_h9mskbvs2jg3xxrlfslk26v2k
foreign key (user_id)
references users;

alter table claims
add constraint FK_a3q00havuvj5bpg0bc0d45vmt
foreign key (ticket_id)
references tickets;

alter table claims
add constraint FK_cloxfm9p5vca3gdd0vkoqt2a5
foreign key (claim_id)
references claims;

alter table games
add constraint FK_5hu6j5f3bl84paaon9tbh3cgh
foreign key (user_id)
references users;

alter table games_users
add constraint FK_inwncxhbv9t8gxc19vebo3xss
foreign key (user_id)
references users;

alter table games_users
add constraint FK_garpn3cyjfiwp800hbo08180o
foreign key (game_id)
references games;

alter table tickets
add constraint FK_ouyie3suiymfn0mmxhm4kdn3l
foreign key (user_id)
references users;

alter table tickets
add constraint FK_thrqriceu1gn62g3cobat0ip3
foreign key (game_id)
references games