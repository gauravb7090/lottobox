const dummyTicket = [
  [2, null, 27, 32, 42, 50, null, null],
  [4, 14, 29, 35, null, null, null, 73],
  [, null, null, null, 43, 57, 66, 76, 83],
];

class Queue {
  constructor() {
    this.arr = [];
  }

  add(num) {
    this.arr.unshift(num);
  }

  remove() {
    return this.arr.pop();
  }

  getLength() {
    return this.arr.length;
  }

  getItens() {
    return this.arr;
  }
}

const lastCheckedNumbers = new Queue();

const lastCheckedNumbersNode = document.querySelector('.last-checked-numbers');

const createTicket = numbers => {
  const table = document.querySelector('.ticket');

  numbers.forEach(row => {
    const rowNode = document.createElement('tr');
    row.forEach(col => {
      const colNode = document.createElement('td');
      colNode.innerText = typeof col === 'number' ? String(col) : '';
      rowNode.appendChild(colNode);
    });

    table.appendChild(rowNode);
  });
};
createTicket(dummyTicket);

const createNumberList = () => {
  const grid = document.querySelector('.all-number-grid');

  let k = 1;
  for (let i = 0; i < 10; i++) {
    const row = document.createElement('tr');

    for (let j = 0; j < 10; j++) {
      const col = document.createElement('td');
      col.innerText = String(k);
      col.id = `num-${k}`;
      row.appendChild(col);
      k++;
    }

    grid.appendChild(row);
  }
};

createNumberList();

const randomNumberRange = [];
for (let i = 0; i < 20; i++) {
  const randomNumber = Math.floor(Math.random() * 100 + 1);
  console.log(randomNumber);
  if (!randomNumberRange.includes(randomNumber)) {
    randomNumberRange.push(randomNumber);
  }
}

console.log({ randomNumberRange });

const mockBackendApiMessages = () => {
  const interval = 2000;

  const popNumbers = setInterval(() => {
    if (!randomNumberRange.length) {
      clearInterval(popNumbers);
      return false;
    }

    const currentNumber = randomNumberRange.pop();

    console.log(currentNumber);
    checkNumbersInGrid(currentNumber);

    updateLastCheckedNumbersQueue(updateQueue(currentNumber).getItens());
  }, interval);
};

const checkNumbersInGrid = num => {
  const currentNode = document.querySelector(`#num-${num}`);
  currentNode.classList.add('checked');
};

const updateQueue = newNumber => {
  const maxLength = 5;
  const currentLength = lastCheckedNumbers.getLength();

  if (currentLength === maxLength) {
    lastCheckedNumbers.remove();
  }

  lastCheckedNumbers.add(newNumber);

  return lastCheckedNumbers;
};

const updateLastCheckedNumbersQueue = numbers => {
  const queueWrapper = document.createElement('div');
  queueWrapper.classList.add('flex');

  numbers.forEach(num => {
    const div = document.createElement('div');
    div.innerText = num;
    div.classList.add('last-num');
    queueWrapper.appendChild(div);
  });

  lastCheckedNumbersNode.innerHTML = '';
  lastCheckedNumbersNode.appendChild(queueWrapper);
};

mockBackendApiMessages();