package handlers;

import com.google.inject.Inject;
import core.GameLogic;
import core.VariationType;
import dao.ClaimDao;
import dao.TicketDao;
import dao.UserDao;
import models.Ticket;
import models.User;
import models.dto.ClaimDto;
import models.dto.TicketClaimDto;
import ninja.jpa.UnitOfWork;
import org.slf4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@UnitOfWork
public class ClaimHandler {

    private final ClaimDao claimDao;
    private final TicketDao ticketDao;
    private final UserDao userDao;
    private final Logger logger;

    @Inject
    public ClaimHandler(ClaimDao claimDao, TicketDao ticketDao, UserDao userDao, Logger logger) {
        this.claimDao = claimDao;
        this.ticketDao = ticketDao;
        this.userDao = userDao;
        this.logger = logger;
    }

    public List<ClaimDto> list() {
        return claimDao.findAll().stream().map(ClaimDto::new).collect(Collectors.toList());
    }

    public Optional<ClaimDto> get(UUID id) {
        return claimDao.find(id).map(ClaimDto::new);
    }

    public Optional<ClaimDto> create(UUID userId, UUID ticketId, VariationType type) {
        final Optional<User> userOpt = userDao.find(userId);
        if (userOpt.isEmpty()) {
            logger.info("No User found {}", userId);
            return Optional.empty();
        }
        final Optional<Ticket> ticketOpt = ticketDao.find(ticketId);
        if (ticketOpt.isEmpty()) {
            logger.info("No Ticket found {}", ticketId);
            return Optional.empty();
        }
        final Ticket ticket = ticketOpt.get();
        final TicketClaimDto ticketClaimDto = new TicketClaimDto(ticket);
        if (!GameLogic.claim(type, ticketClaimDto)) {
            logger.info("Invalid claim");
            return Optional.empty();
        }
        final List<ClaimDto> claimList = claimDao.findAll().stream().map(ClaimDto::new).collect(Collectors.toList());
        Set<ClaimDto> similarClaims = claimList.stream()
                .filter(claim -> claim.getType() == type && claim.getGameId() == ticketClaimDto.getGame().getId())
                .collect(Collectors.toSet());
        if (similarClaims.size() > 0) {
            return Optional.empty();
        }
        return Optional.of(claimDao.create(userOpt.get(), ticket, type))
                .map(ClaimDto::new);
    }
}
