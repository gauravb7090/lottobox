package handlers;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import core.CookieLogic;
import dao.UserDao;
import models.User;
import models.dto.UserDto;
import ninja.Context;
import ninja.jpa.UnitOfWork;
import org.slf4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static etc.Constants.AUTH_KEY;

@UnitOfWork
public class UserHandler {

    private final UserDao userDao;
    private final Logger logger;

    @Inject
    public UserHandler(UserDao userDao, Logger logger) {
        this.userDao = userDao;
        this.logger = logger;
    }

    public List<UserDto> list() {
        return userDao.findAll()
                .stream()
                .map(UserDto::new)
                .collect(Collectors.toList());

    }

    private User create(String name, Context context) {
        return userDao.create(name);
    }

    public UserDto createAndSetCookie(String name, Context context) {
        final UserDto userDto = new UserDto(create(name, context));
        logger.info("Created user with id {}", userDto.getId());
        CookieLogic.setCookie(AUTH_KEY, userDto.getId(), context);
        return userDto;
    }

    public UserDto getOrCreate(String name, Context context) {
        final Optional<UserDto> userByCookie = getUserByCookie(context);
        if (userByCookie.isEmpty()) {
            return createAndSetCookie(name, context);
        }else {
            return userByCookie.get();
        }
    }

    public Optional<UserDto> getUserByCookie(Context context) {
        final boolean isAuthKeyPresent = (context.getSession() != null && !Strings.isNullOrEmpty(context.getSession().get(AUTH_KEY)));
        if (isAuthKeyPresent) {
            String authKey = context.getSession().get(AUTH_KEY);
            logger.error("TRYING BY UUID {}", authKey);
            final UUID userId;
            try {
                userId = UUID.fromString(authKey);
                final Optional<UserDto> userDtoOpt = getById(userId);
                if (userDtoOpt.isEmpty()) {
                    logger.error("User not found with valid UUID in AUTH_KEY");
                    return Optional.empty();
                } else {
                    return userDtoOpt;
                }
            } catch (IllegalArgumentException ex) {
                logger.error("Invalid UUID in AUTH_KEY");
                return Optional.empty();
            }
        }
        logger.error("TRYING FAILED");
        return Optional.empty();
    }

    public Optional<UserDto> getById(UUID uuid) {
        final Optional<User> userV2Opt = userDao.find(uuid);
        return userV2Opt.map(UserDto::new);
    }
}
