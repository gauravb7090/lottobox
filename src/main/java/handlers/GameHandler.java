package handlers;

import com.google.common.base.Strings;
import core.*;
import dao.GameDao;
import dao.UserDao;
import models.Game;
import models.Ticket;
import models.User;
import models.dto.GameDto;
import models.dto.GameStatus;
import models.dto.TicketDto;
import models.dto.UserDto;
import models.dto.request.WSRequestType;
import ninja.Context;
import ninja.jpa.UnitOfWork;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.websocket.Session;
import java.util.*;
import java.util.stream.Collectors;

import static etc.Constants.GAME_KEY;

@UnitOfWork
public class GameHandler {

    private final GameWSPublisher gameWSPublisher;
    private final UserState userState;
    private final TicketState ticketState;
    private final GameDao gameDao;
    private final UserDao userDao;
    private final TicketHandler ticketHandler;
    private final ClaimHandler claimHandler;
    private final Logger logger;

    @Inject
    public GameHandler(GameWSPublisher gameWSPublisher, UserState userState, TicketState ticketState, GameDao gameDao, UserDao userDao, TicketHandler ticketHandler, ClaimHandler claimHandler, Logger logger) {
        this.gameWSPublisher = gameWSPublisher;
        this.userState = userState;
        this.ticketState = ticketState;
        this.gameDao = gameDao;
        this.userDao = userDao;
        this.ticketHandler = ticketHandler;
        this.claimHandler = claimHandler;
        this.logger = logger;
    }

    public List<GameDto> list() {
        return gameDao.findAll().stream().map(GameDto::new).collect(Collectors.toList());
    }

    public Optional<GameDto> get(UUID id) {
        return gameDao.find(id).map(GameDto::new);
    }

    public Optional<GameDto> getGameByCookie(Context context) {
        final boolean isGameKeyPresent = (context.getSession() != null && !Strings.isNullOrEmpty(context.getSession().get(GAME_KEY)));
        if (isGameKeyPresent) {
            String gameKey = context.getSession().get(GAME_KEY);
            logger.error("TRYING BY UUID {}", gameKey);
            final UUID gameId;
            try {
                gameId = UUID.fromString(gameKey);
                final Optional<GameDto> gameDtoOpt = get(gameId);
                if (gameDtoOpt.isEmpty()) {
                    logger.error("Game not found with valid UUID in GAME_KEY");
                    return Optional.empty();
                } else {
                    return gameDtoOpt;
                }
            } catch (IllegalArgumentException ex) {
                logger.error("Invalid UUID in GAME_KEY");
                return Optional.empty();
            }
        }
        logger.error("TRYING FAILED");
        return Optional.empty();
    }

    public Optional<GameDto> create(UserDto host, Context context) {
        final User user = userDao.find(host.getId()).get();
        final Optional<Game> gameOpt = gameDao.create(user);
        final Optional<GameDto> gameDtoOpt = gameOpt.map(GameDto::new);
        gameDtoOpt.ifPresent(gameDto -> gameOpt.ifPresent(game -> {
            final TicketDto ticketDto = assignTicket(game, user);
            logger.info("Created ticket {} belonging to game {} for user {}", ticketDto.getId(), game.getId(), user.getId());
            CookieLogic.setCookie(GAME_KEY, game.getId(), context);
            publish(WSRequestType.NewUserJoin, gameDto, host.getId());
        }));
        return gameDtoOpt;
    }

    public Optional<GameDto> joinGame(String code, UserDto userDto, Context context) {
        final User user = userDao.find(userDto.getId()).get();
        final Optional<Game> gameV2Opt = gameDao.findByCode(code);
        if (gameV2Opt.isPresent()) {
            Game game = gameV2Opt.get();
            final GameDto gameDto;
            logger.info("Game found with id {}", game.getId());
            if (!game.getMembers().contains(user)) {
                game.getMembers().add(user);
                final Optional<Ticket> anyTicketAlready = game.getTickets()
                        .stream()
                        .filter(ticket ->
                                ticket.getOwnedBy().getId() == user.getId() &&
                                        ticket.getPartOf().getId() == game.getId())
                        .findAny();
                if (anyTicketAlready.isEmpty()) {
                    final TicketDto ticketDto = assignTicket(game, user);
                    logger.info("Created ticket {} belonging to game {} for user {}", ticketDto.getId(), game.getId(), user.getId());
                }
                logger.info("Added member {} to game {}", user.getId(), game.getId());
                gameDao.update(game);
                CookieLogic.setCookie(GAME_KEY, game.getId(), context);
                gameDto = new GameDto(game);
                publish(WSRequestType.NewUserJoin, gameDto, userDto.getId());
            }else {
                logger.info("Member {} already part of game {}", user.getId(), game.getId());
                CookieLogic.setCookie(GAME_KEY, game.getId(), context);
                gameDto = new GameDto(game);
            }
            return Optional.of(gameDto);
        }
        return Optional.empty();
    }

    private TicketDto assignTicket(Game game, User user) {
        return ticketHandler.create(user, game);
    }

    public Optional<GameDto> startGame(UUID gameId, UserDto userDto) {
        final Optional<Game> gameV2 = gameDao.find(gameId);
        gameV2.ifPresent(game -> {
            logger.info("Game found with id {}", gameId);
            if (game.getHost().getId().equals(userDto.getId())) {
                logger.info("Host {} found for game {}", userDto.getId(), gameId);
                if (game.getStatus().equals(GameStatus.INACTIVE)) {
                    game.setStatus(GameStatus.ACTIVE);
                    gameDao.update(game);
                } else {
                    if (game.getStatus().equals(GameStatus.ACTIVE)) {
                        logger.info("Game {} already active", gameId);
                    }
                    if (game.getStatus().equals(GameStatus.COMPLETE)) {
                        logger.info("Game {} already complete", gameId);
                    }
                }
            }
        });
        final Optional<GameDto> gameDtoOpt = gameV2.map(GameDto::new);
        gameDtoOpt.ifPresent(gameDto -> this.publish(WSRequestType.GameStarted, gameDto, userDto.getId()));
        return gameDtoOpt;
    }

    public void publish(WSRequestType requestType, GameDto gameDto, UUID callerId) {
        gameWSPublisher.publish(requestType, gameDto, callerId);
    }

    public Optional<GameDto> addSubscriber(UUID gameId, UUID userId, Session session) {
        logger.info("Adding subscriber {} to {} with {}", userId, gameId, session.getId());
        final Optional<GameDto> gameDtoOpt = get(gameId);
        if(gameDtoOpt.isPresent()) {
            logger.info("Game Found for id {}", gameId);
            final GameDto gameDto = gameDtoOpt.get();
            logger.info("Game {} details - {}", gameId, gameDto);
            final Set<UUID> memberIdSet =
                    gameDto.getMembers().stream()
                            .map(UserDto::getId)
                            .collect(Collectors.toSet());
            logger.info("Members {}",Arrays.toString(memberIdSet.toArray()));
            if (memberIdSet.contains(userId)) {
                logger.info("User {} exists in Members {}",userId, Arrays.toString(memberIdSet.toArray()));
                gameWSPublisher.addSubscriber(gameId, userId, session);
                publish(WSRequestType.NewUserJoin, gameDto, userId);
            }
        }else {
            logger.info("No Game Found for id {}", gameId);
        }
        return gameDtoOpt;
    }

    public void removeSubscriber(UUID gameId, UUID userId) {
        gameWSPublisher.removeSubscriber(gameId, userId);
    }

    public void generateNumber(UUID gameId, UUID userId) {
        logger.info("Generating number for {}", gameId);
        final Optional<GameDto> gameOpt = gameDao.incrementCounter(gameId, userId).map(GameDto::new);
        gameOpt.ifPresent(gameDto -> publish(WSRequestType.GenerateNumber, gameDto, userId));
    }

    public Optional<GameDto> generateAllNumbers(UUID gameId, UUID userId) {
        logger.info("Generating all numbers for {}", gameId);
        final Optional<GameDto> gameOpt = gameDao.updateCounterToEnd(gameId, userId).map(GameDto::new);
        gameOpt.ifPresent(gameDto -> publish(WSRequestType.GenerateNumber, gameDto, userId));
        return gameOpt;
    }

    public void checkNumber(UUID gameId, UUID userId, int number, boolean checkFlag) {
        final Optional<GameDto> gameDtoOpt = get(gameId);
        gameDtoOpt.ifPresent(gameDto -> {
            logger.info("Trying to check number {} for gameId {} userId {}", number, gameId, userId);
            ticketState.checkNumber(gameDto, userId, number, checkFlag);
            publish(WSRequestType.NumbersChecked, gameDto, userId);
        });
    }

    public void claim(UUID gameId, UUID userId, VariationType type) {
        logger.info("Trying to claim {} by user {} for game {}", type, userId, gameId);
        final Optional<GameDto> gameDtoOpt = get(gameId);
        gameDtoOpt.ifPresent(gameDto -> {
            logger.info("Game {} found", gameDto.getId());
            final Optional<TicketDto> possibleTicket = gameDto.getTickets().stream().filter(ticketDto -> ticketDto.getUserId().equals(userId)).findFirst();
            possibleTicket.ifPresent(ticketDto -> {
                logger.info("Ticket {} found", ticketDto.getId());
                claimHandler.create(userId, ticketDto.getId(), type);
            });
            publish(WSRequestType.Claim, gameDto, userId);
        });
    }

    public Optional<Session> getWSSession(UUID gameId, UUID userId) {
        return userState.getSession(gameId, userId);
    }
}
