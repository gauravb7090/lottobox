package handlers;

import com.google.inject.Inject;
import core.TicketLogic;
import dao.GameDao;
import dao.TicketDao;
import dao.UserDao;
import models.Game;
import models.Ticket;
import models.User;
import models.dto.TicketDto;
import models.dto.UserDto;
import ninja.jpa.UnitOfWork;
import org.slf4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@UnitOfWork
public class TicketHandler {

    private final TicketLogic logic;
    private final TicketDao ticketDao;
    private final UserDao userDao;
    private final GameDao gameDao;
    private final Logger logger;

    @Inject
    public TicketHandler(TicketLogic logic, TicketDao ticketDao, UserDao userDao, GameDao gameDao, Logger logger) {
        this.logic = logic;
        this.ticketDao = ticketDao;
        this.userDao = userDao;
        this.gameDao = gameDao;
        this.logger = logger;
    }

    public List<TicketDto> list() {
        return ticketDao.findAll().stream().map(TicketDto::new).collect(Collectors.toList());
    }

    public Optional<TicketDto> get(UUID id) {
        return ticketDao.find(id).map(TicketDto::new);
    }

    public TicketDto create(UserDto userDto, UUID gameId) {
        final Optional<User> user = userDao.find(userDto.getId());
        final Optional<Game> game = gameDao.find(gameId);
        final int[][] genTicket = logic.getTicket();
        final Ticket ticket = ticketDao.create(user.get(), game.get(), genTicket);
        return new TicketDto(ticket);
    }

    public TicketDto create(User user, Game game) {
        final int[][] genTicket = logic.getTicket();
        final Ticket ticket = ticketDao.create(user, game, genTicket);
        return new TicketDto(ticket);
    }
}
