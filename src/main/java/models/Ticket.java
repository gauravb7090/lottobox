package models;

import com.google.common.collect.Sets;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "tickets")
public class Ticket extends TrackedEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    @Column(name = "ticket_id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "ticket_matrix", updatable = false, nullable = false)
    private int[][] numbers;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id", nullable = false)
    private Game partOf;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User ownedBy;

    @ManyToMany(mappedBy = "ticket", fetch = FetchType.EAGER)
    private Set<Claim> claims = Sets.newHashSet();

    public Ticket(User user, Game game, int[][] numbers) {
        this.ownedBy = user;
        this.partOf = game;
        this.numbers = numbers;
    }

    public Ticket() {
    }

    public UUID getId() {
        return id;
    }

    public int[][] getNumbers() {
        return numbers;
    }

    public Game getPartOf() {
        return partOf;
    }

    public User getOwnedBy() {
        return ownedBy;
    }

}