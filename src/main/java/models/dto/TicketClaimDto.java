package models.dto;

import com.google.common.collect.Maps;
import core.VariationType;
import models.Ticket;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class TicketClaimDto {

    private final UUID id;

    private final GameDto game;

    private final int[][] numbers;

    private final Map<VariationType, VariationsDto> variationsMap = Maps.newEnumMap(VariationType.class);

    public TicketClaimDto(Ticket ticket) {
        this.id = ticket.getId();
        this.game = new GameDto(ticket.getPartOf());
        this.numbers = ticket.getNumbers();
        setVariations();
    }

    private void setVariations() {
        for (VariationType vType: VariationType.values()) {
            variationsMap.put(vType, new VariationsDto(vType, numbers));
        }
    }

    public UUID getId() {
        return id;
    }

    public GameDto getGame() {
        return game;
    }

    public Optional<VariationsDto> getVariation(VariationType type) {
        return Optional.ofNullable(variationsMap.get(type));
    }

    public List<VariationsDto> getVariations() {
        return variationsMap
                .entrySet()
                .stream()
                .map(entry -> entry.getValue())
                .collect(Collectors.toList());
    }
}