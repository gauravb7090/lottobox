package models.dto.request;

public class WSCheckNumberRequest extends AuthWSRequest {

    public int number = -1;

    public boolean isChecked = true;
}
