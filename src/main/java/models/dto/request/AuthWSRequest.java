package models.dto.request;

import org.hibernate.validator.constraints.NotBlank;

public class AuthWSRequest {

    @NotBlank
    public String userId;

    @NotBlank
    public String gameId;
}
