package models.dto.request;

import org.hibernate.validator.constraints.NotBlank;

public class StartGameRequest {

    @NotBlank
    public String gameId;

}
