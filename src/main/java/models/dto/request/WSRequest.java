package models.dto.request;

import org.hibernate.validator.constraints.NotBlank;

public class WSRequest {

    public WSRequestType requestType;

    @NotBlank
    public String payload;

}
