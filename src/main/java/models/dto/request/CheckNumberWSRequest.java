package models.dto.request;

public class CheckNumberWSRequest extends AuthWSRequest {

    public int number = -1;

    public boolean isChecked = true;
}
