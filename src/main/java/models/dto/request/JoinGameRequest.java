package models.dto.request;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;

public class JoinGameRequest {

    @NotBlank
    public String name;

    @NotBlank
    @Pattern(regexp = "\\d{6}", message = "Invalid Game Code")
    public String gameCode;

}
