package models.dto.request;

import org.hibernate.validator.constraints.NotBlank;

public class CreateGameRequest {

    @NotBlank
    public String name;

}
