package models.dto.request;

public enum WSRequestType {
    Heartbeat,
    GameStarted,
    NewUserJoin,
    GenerateNumber,
    Claim,
    NumbersChecked
}
