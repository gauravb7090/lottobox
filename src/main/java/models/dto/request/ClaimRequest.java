package models.dto.request;

import core.VariationType;
import org.hibernate.validator.constraints.NotBlank;

public class ClaimRequest {

    @NotBlank
    public String ticketId;

    @NotBlank
    public VariationType type;
}
