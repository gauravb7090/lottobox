package models.dto;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Value;
import models.User;

import java.util.UUID;

@Value
@JsonView(View.Public.class)
public class UserDto extends TrackedEntityDto {

    private final UUID id;

    private final String name;

    public UserDto(User dto) {
        super(dto);
        this.id = dto.getId();
        this.name = dto.getName();
    }
}
