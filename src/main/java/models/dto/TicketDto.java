package models.dto;

import com.fasterxml.jackson.annotation.JsonView;
import models.Ticket;

import java.util.UUID;

public class TicketDto extends TrackedEntityDto {

    @JsonView(View.Public.class)
    private final UUID id;

    @JsonView(View.Public.class)
    private final int[][] numbers;

    @JsonView(View.Public.class)
    private final UUID userId;

    @JsonView(View.Public.class)
    private final UUID gameId;

    public TicketDto(Ticket ticket) {
        super(ticket);
        this.id = ticket.getId();
        this.numbers = ticket.getNumbers();
        this.userId = ticket.getOwnedBy().getId();
        this.gameId = ticket.getPartOf().getId();
    }

    public UUID getId() {
        return id;
    }

    public int[][] getNumbers() {
        return numbers;
    }

    public UUID getUserId() {
        return userId;
    }

    public UUID getGameId() {
        return gameId;
    }

    public boolean containsNumber(int number) {
        if (number <= 0) {
            return false;
        }
        for (int[] ints : numbers) {
            for (int j = 0; j < numbers[0].length; j++) {
                if (ints[j] == number) {
                    return true;
                }
            }
        }
        return false;
    }
}