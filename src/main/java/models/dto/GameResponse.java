package models.dto;

import com.fasterxml.jackson.annotation.JsonView;

import java.util.List;
import java.util.UUID;

public class GameResponse {

    @JsonView(View.Public.class)
    private final UUID gameId;
    @JsonView(View.Public.class)
    private final List<Integer> numbers;
    @JsonView(View.Public.class)
    private final int newNumber;

    public GameResponse(GameDto gameDto, List<Integer> numbers) {
        this.gameId = gameDto.getId();
        this.numbers = numbers;
        if (numbers != null && numbers.size() > 0) {
            this.newNumber = numbers.get(numbers.size()-1);
        }
        else {
            newNumber = 0;
        }
    }
}