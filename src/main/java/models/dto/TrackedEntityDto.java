package models.dto;

import com.fasterxml.jackson.annotation.JsonView;
import models.TrackedEntity;

import java.sql.Timestamp;

public abstract class TrackedEntityDto {

    @JsonView(View.Public.class)
    private Timestamp createdAt;

    @JsonView(View.Public.class)
    private Timestamp updatedAt;

    public TrackedEntityDto(TrackedEntity entity) {
        this.createdAt = entity.getCreatedAt();
        this.updatedAt = entity.getUpdatedAt();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }
}
