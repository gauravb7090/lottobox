package models.dto;

import com.google.common.collect.Sets;
import core.Variation;
import core.VariationType;
import org.apache.commons.lang3.tuple.Triple;

import java.util.Set;

public class VariationsDto {

    private final Set<Integer> numbers = Sets.newHashSet();
    private final Variation type;

    public VariationsDto(VariationType vType, int[][] ticket) {
        this.type = Variation.fromType(vType);
        for (Triple<Variation.TRAVERSAL_TYPE,Integer, Integer> v: type.getTraversal()) {
            Variation.TRAVERSAL_TYPE traversal_type = v.getLeft();
            int row = v.getMiddle();
            int col = v.getRight();
            if (traversal_type == Variation.TRAVERSAL_TYPE.ROW) {
                for (int j = 0; j < col; j++) {
                    final int val = ticket[row][j];
                    if (val > 0) {
                        numbers.add(val);
                    }
                }
            }
            if (traversal_type == Variation.TRAVERSAL_TYPE.COL) {
                for (int i = 0; i < row; i++) {
                    final int val = ticket[i][col];
                    if (val > 0) {
                        numbers.add(val);
                    }
                }
            }
        }
    }

    public Set<Integer> getNumbers() {
        return numbers;
    }

    @Override
    public String toString() {
        return "VariationsDto{" +
                "numbers=" + numbers +
                ", type=" + type +
                '}';
    }
}
