package models.dto;

import com.fasterxml.jackson.annotation.JsonView;
import core.VariationType;
import models.Claim;

import java.util.UUID;

public class ClaimDto extends TrackedEntityDto {

    @JsonView(View.Public.class)
    private final UUID id;

    @JsonView(View.Public.class)
    private final UserDto user;

    @JsonView(View.Public.class)
    private final UUID gameId;

    @JsonView(View.Public.class)
    private final VariationType type;

    public ClaimDto(Claim claim) {
        super(claim);
        this.id = claim.getId();
        this.user = new UserDto(claim.getOwner());
        this.gameId = claim.getTicket().getPartOf().getId();
        this.type = claim.getType();
    }

    public UUID getId() {
        return id;
    }

    public UserDto getUser() {
        return user;
    }

    public UUID getGameId() {
        return gameId;
    }

    public VariationType getType() {
        return type;
    }
}