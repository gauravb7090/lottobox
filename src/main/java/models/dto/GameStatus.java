package models.dto;

public enum GameStatus {
    INACTIVE,
    ACTIVE,
    COMPLETE
}
