package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import models.dto.GameStatus;
import models.dto.View;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@Builder
@JsonView(View.Public.class)
public class ApiResponse {
    private final UUID gameId;
    private final String gameCode;
    private final long reconnectThreshold;
    private final long claimingWindow;
    private final GameStatus status;
    private final List<Integer> numbers;
    private final List<UserResponse> players;
    private final List<ClaimResponse> claims;

    // caller data
    private final UUID userId;
    private final Set<Integer> numbersChecked;
    private final TicketResponse ticket;
}