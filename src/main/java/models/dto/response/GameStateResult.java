package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import models.dto.GameStatus;
import models.dto.View;

import java.util.List;

@AllArgsConstructor
@Builder
@JsonView(View.Public.class)
public class GameStateResult {

    private final List<Integer> numbers;
    private final GameStatus status;

}
