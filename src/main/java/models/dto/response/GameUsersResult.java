package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import models.dto.View;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Builder
@JsonView(View.Public.class)
public class GameUsersResult {

    private final UUID gameId;

    private final List<GameUserResponse> users;

}
