package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import models.dto.GameStatus;
import models.dto.UserDto;
import models.dto.View;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Builder
public class NewGameResult {

    @JsonView(View.Public.class)
    private final UUID userId;
    @JsonView(View.Public.class)
    private final UUID gameId;
    @JsonView(View.Public.class)
    private final String gameCode;
    @JsonView(View.Public.class)
    private final long reconnectThreshold;
    @JsonView(View.Public.class)
    private final long claimingWindow;
    @JsonView(View.Public.class)
    private final GameStatus status;
    @JsonView(View.Public.class)
    private final List<UserDto> players;

}
