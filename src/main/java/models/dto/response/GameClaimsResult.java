package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import models.dto.View;

import java.util.List;

@AllArgsConstructor
@Builder
@JsonView(View.Public.class)
public class GameClaimsResult {

    private final List<GameClaimResponse> claims;

}