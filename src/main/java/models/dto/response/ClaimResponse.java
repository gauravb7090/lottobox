package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import core.VariationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import models.dto.View;

import java.util.UUID;

@AllArgsConstructor
@Builder
@JsonView(View.Public.class)
public class ClaimResponse {

    private final VariationType type;
    private final UserResponse claimedBy;

}
