package models.dto.response;

public enum PlayerType {
    PLAYER,
    HOST
}
