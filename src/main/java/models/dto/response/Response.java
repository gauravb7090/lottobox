package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.base.Strings;
import lombok.Builder;
import lombok.Getter;
import models.dto.View;

@Getter
public class Response<T> {

    @JsonView
    private final T result;

    @JsonView(View.Public.class)
    private String error;

    @Builder(builderMethodName = "successBuilder")
    public Response(T result) {
        this.result = result;
        this.error = Strings.nullToEmpty(null);
    }

    @Builder(builderMethodName = "errorBuilder")
    public Response(T result, String error) {
        this.result = result;
        this.error = Strings.nullToEmpty(error);
    }
}