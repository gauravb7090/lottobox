package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import models.dto.View;

import java.util.Set;

@AllArgsConstructor
@Builder
@JsonView(View.Public.class)
public class GameTicketResult {

    private final int[][] numbers;
    private final Set<Integer> numbersChecked;

}
