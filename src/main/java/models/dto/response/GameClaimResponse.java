package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import core.VariationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import models.dto.UserDto;
import models.dto.View;

@AllArgsConstructor
@Builder
@JsonView(View.Public.class)
public class GameClaimResponse {

    private final UserDto user;
    private final VariationType type;

}
