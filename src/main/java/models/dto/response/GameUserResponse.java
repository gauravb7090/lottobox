package models.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import models.dto.View;

import java.util.UUID;

@AllArgsConstructor
@Builder
@JsonView(View.Public.class)
public class GameUserResponse {

    private final UUID userId;
    private final String name;
    private final PlayerType playerType;
    private final boolean connectionStatus;
}
