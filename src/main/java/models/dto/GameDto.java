package models.dto;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.EqualsAndHashCode;
import lombok.Value;
import models.Game;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Value
@JsonView(View.Public.class)
public class GameDto extends TrackedEntityDto {

    UUID id;

    String code;

    int row;

    int col;

    GameStatus status;

    long reconnectThreshold;

    long claimingWindow;

    @JsonView(View.Private.class)
    List<Integer> numbers;

    @JsonView(View.Private.class)
    int numberIdx;

    UserDto host;

    List<UserDto> members;

    List<TicketDto> tickets;

    List<ClaimDto> claims;

    public GameDto(Game dto) {
        super(dto);
        this.id = dto.getId();
        this.code = dto.getCode();
        this.row = dto.getRow();
        this.col = dto.getCol();
        this.status = dto.getStatus();
        this.reconnectThreshold = dto.getReconnectThreshold();
        this.claimingWindow = dto.getClaimingWindow();
        this.numbers = dto.getNumbers();
        this.numberIdx = dto.getNumberIdx();
        this.host = new UserDto(dto.getHost());
        this.members = dto.getMembers().stream().map(UserDto::new).collect(Collectors.toList());
        this.tickets = dto.getTickets().stream().map(TicketDto::new).collect(Collectors.toList());
        this.claims = dto.getClaims().stream().map(ClaimDto::new).collect(Collectors.toList());
    }
}