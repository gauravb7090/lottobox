package models;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import models.dto.GameStatus;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "games")
public class Game extends TrackedEntity implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    @Column(name = "game_id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "game_code", nullable = false, unique = true)
    private String code;

    @Column(name = "row", nullable = false)
    private int row = 3;

    @Column(name = "col", nullable = false)
    private int col = 9;

    @Column(name = "status", nullable = false)
    private GameStatus status = GameStatus.INACTIVE;

    @Column(name = "reconnect_threshold", nullable = false)
    private long reconnectThreshold = 15000;

    @Column(name = "claiming_window", nullable = false)
    private long claimingWindow = 15000;

    @Column(name = "generationWindow", nullable = false)
    private long generationWindow = 5000;

    @Lob
    private ArrayList<Integer> numbers = Lists.newArrayList();

    @Column(name = "numberIdx", nullable = false)
    private int numberIdx = -1;

    @Column(name = "numberGeneratedAt", nullable = false)
    private Timestamp numberGeneratedAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private User host;

    @EqualsAndHashCode.Exclude
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "games_users",
            joinColumns = { @JoinColumn(name = "game_id") },
            inverseJoinColumns = { @JoinColumn(name = "user_id") }
    )
    private Set<User> members = Sets.newHashSet();

    @OneToMany(mappedBy = "partOf", fetch = FetchType.EAGER)
    private Set<Ticket> tickets = Sets.newHashSet();

    @OneToMany(mappedBy = "gameRef", fetch = FetchType.EAGER)
    private Set<Claim> claims = Sets.newHashSet();

}