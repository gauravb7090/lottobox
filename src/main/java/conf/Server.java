package conf;

import ninja.standalone.NinjaJetty;
import ninja.utils.NinjaMode;

public class Server {
    public static void main(String[] args) {
        new NinjaJetty().ninjaMode(NinjaMode.dev).run();
    }
}
