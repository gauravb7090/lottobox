package conf;

import com.google.inject.Inject;
import controllers.*;
import etc.JsonFilter;
import ninja.AssetsController;
import ninja.Router;
import ninja.application.ApplicationRoutes;
import ninja.utils.NinjaProperties;

public class Routes implements ApplicationRoutes {
    
    @Inject
    NinjaProperties ninjaProperties;

    /**
     * Using a (almost) nice DSL we can configure the router.
     * 
     * The second argument NinjaModuleDemoRouter contains all routes of a
     * submodule. By simply injecting it we activate the routes.
     * 
     * @param router
     *            The default router of this application
     */
    @Override
    public void init(Router router) {  

        // puts test data into db:
        if (!ninjaProperties.isProd()) {
            router.GET().route("/setup").with(ApplicationController::setup);
        }

        router.GET().route("/games").with(GameController::list);
        router.POST().route("/games").with(GameController::hostGame);
        router.POST().route("/games/join").with(GameController::joinGame);
        router.POST().route("/games/start").with(GameController::startGame);
        router.POST().route("/games/all").with(GameController::generateAllNumbers);

        router.WS().route("/{userId}/game/{gameId}").with(GameWSController::handshake);

        // Monitoring APIs
        router.GET().route("/users").with(UserController::list);

        router.GET().route("/tickets").with(TicketController::list);
        router.GET().route("/tickets/{id}").with(TicketController::get);
        router.POST().filters(JsonFilter.class).route("/tickets/{gameId}").with(TicketController::create);

        router.GET().route("/claims").with(ClaimController::list);
        router.GET().route("/claims/{id}").with(ClaimController::get);
        router.POST().filters(JsonFilter.class).route("/claims").with(ClaimController::create);
 
        ///////////////////////////////////////////////////////////////////////
        // Assets (pictures / javascript)
        ///////////////////////////////////////////////////////////////////////    
        router.GET().route("/assets/webjars/{fileName: .*}").with(AssetsController::serveWebJars);
        router.GET().route("/assets/{fileName: .*}").with(AssetsController::serveStatic);
        
        ///////////////////////////////////////////////////////////////////////
        // Index / Catchall shows index page
        ///////////////////////////////////////////////////////////////////////
        router.GET().route("/.*").with(ApplicationController::index);
    }

}
