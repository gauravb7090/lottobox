package conf;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import core.GameLogic;

@Singleton
public class Module extends AbstractModule {
    protected void configure() {
        bind(StartupActions.class);
        requestStaticInjection(GameLogic.class);
    }
}
