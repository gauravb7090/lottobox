package dao;

import com.google.inject.Provider;
import core.VariationType;
import models.Claim;
import models.Ticket;
import models.User;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class ClaimDao extends AbstractDao<Claim> {

    public final Logger logger;

    @Inject
    public ClaimDao(Provider<EntityManager> entityManagerProvider, Logger logger) {
        super(entityManagerProvider, logger);
        setClazz(Claim.class);
        this.logger = logger;
    }

    public Claim create(User user, Ticket ticket, VariationType type) {
        Claim claim = new Claim(user, ticket, type);
        save(claim);
        return claim;
    }
}
