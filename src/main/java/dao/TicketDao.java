package dao;

import com.google.inject.Provider;
import models.Game;
import models.Ticket;
import models.User;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class TicketDao extends AbstractDao<Ticket> {

    public final Logger logger;

    @Inject
    public TicketDao(Provider<EntityManager> entityManagerProvider, Logger logger) {
        super(entityManagerProvider, logger);
        setClazz(Ticket.class);
        this.logger = logger;
    }

    public Ticket create(User user, Game game, int [][] numbers) {
        Ticket ticket = new Ticket(user, game, numbers);
        save(ticket);
        return ticket;
    }
}
