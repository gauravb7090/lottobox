package dao;

import com.google.common.collect.Lists;
import com.google.inject.Provider;
import core.GameLogic;
import models.Claim;
import models.Game;
import models.Game_;
import models.User;
import models.dto.GameStatus;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.util.*;

public class GameDao extends AbstractDao<Game> {

    public final Logger logger;

    @Inject
    public GameDao(Provider<EntityManager> entityManagerProvider, Logger logger) {
        super(entityManagerProvider, logger);
        setClazz(Game.class);
        this.logger = logger;
    }

    public Optional<Game> create(User host) {
        int tries = 10;
        while (tries > 0) {
            final String code = RandomStringUtils.randomNumeric(6);
            if (findByCode(code).isEmpty()) {
                final Random randSeed = new Random(System.currentTimeMillis());
                final ArrayList<Integer> gameNumbers = Lists.newArrayList(GameLogic.GAME_NUMBERS);
                Collections.shuffle(gameNumbers, randSeed);
                Game game = new Game();
                game.setNumbers(gameNumbers);
                game.setCode(code);
                game.setHost(host);
                game.getMembers().add(host);
                game.setNumberGeneratedAt(new Timestamp(System.currentTimeMillis()));
                save(game);
                return Optional.of(game);
            }
            tries--;
        }
        return Optional.empty();
    }

    public Optional<Game> findByCode(String code) {
        final CriteriaBuilder criteria = getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<Game> query = criteria.createQuery(Game.class);
        final Root<Game> root = query.from(Game.class);
        query.where(criteria.equal(root.get(Game_.code), code));
        return execute(query);
    }

    public Optional<Game> incrementCounter(UUID gameId, UUID userId) {
        final Optional<Game> gameOpt = find(gameId);
        if (gameOpt.isPresent()) {
            final Game game = gameOpt.get();
            if (!game.getStatus().equals(GameStatus.ACTIVE)) {
                return Optional.empty();
            }
            if (!game.getHost().getId().equals(userId)) {
                return Optional.empty();
            }
            final Optional<Timestamp> lastClaimTimeOpt
                    = game.getClaims().stream().map(Claim::getCreatedAt).max(Timestamp::compareTo);
            final Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

            if (lastClaimTimeOpt.isPresent()) {
                final Timestamp lastClaimTime = lastClaimTimeOpt.get();
                final long timeDiff = currentTimestamp.getTime() - lastClaimTime.getTime();

                if (currentTimestamp.before(lastClaimTime) || timeDiff < game.getClaimingWindow()) {
                    logger.info("Not generating NewNumber because of claimWindow");
                    logger.info("current {} lastClaim {} window {}", currentTimestamp, lastClaimTime, game.getClaimingWindow());
                    return Optional.empty();
                }
            }
            final Timestamp lastNumberGeneratedAt = game.getNumberGeneratedAt();
            final long timeDiff = currentTimestamp.getTime() - lastNumberGeneratedAt.getTime();

            if (currentTimestamp.before(lastNumberGeneratedAt) || timeDiff < game.getGenerationWindow()) {
                logger.info("Not generating NewNumber because of generationWindow");
                logger.info("current {} lastNumber {} window {}", currentTimestamp, lastNumberGeneratedAt, game.getGenerationWindow());
                return Optional.empty();
            }
            final int lastNumIdx = game.getNumberIdx();
            final int newNumIdx = lastNumIdx + 1;
            logger.info("newNumIdx {}, numbers {}", lastNumIdx, game.getNumbers());
            if (newNumIdx < game.getNumbers().size()) {
                game.setNumberIdx(newNumIdx);
                game.setNumberGeneratedAt(new Timestamp(System.currentTimeMillis()));
                update(game);
            }
            return Optional.of(game);
        }
        return Optional.empty();
    }

    public Optional<Game> updateCounterToEnd(UUID gameId, UUID userId) {
        final Optional<Game> gameOpt = find(gameId);
        if (gameOpt.isPresent()) {
            final Game game = gameOpt.get();
            if (!game.getStatus().equals(GameStatus.ACTIVE)) {
                return Optional.empty();
            }
            if (!game.getHost().getId().equals(userId)) {
                return Optional.empty();
            }
            final Optional<Timestamp> lastClaimTimeOpt
                    = game.getClaims().stream().map(Claim::getCreatedAt).max(Timestamp::compareTo);
            final Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

            if (lastClaimTimeOpt.isPresent()) {
                final Timestamp lastClaimTime = lastClaimTimeOpt.get();
                final long timeDiff = currentTimestamp.getTime() - lastClaimTime.getTime();

                if (currentTimestamp.before(lastClaimTime) || timeDiff < game.getClaimingWindow()) {
                    logger.info("Not generating NewNumber because of claimWindow");
                    logger.info("current {} lastClaim {} window {}", currentTimestamp, lastClaimTime, game.getClaimingWindow());
                    return Optional.empty();
                }
            }
            final Timestamp lastNumberGeneratedAt = game.getNumberGeneratedAt();
            final long timeDiff = currentTimestamp.getTime() - lastNumberGeneratedAt.getTime();

            if (currentTimestamp.before(lastNumberGeneratedAt) || timeDiff < game.getGenerationWindow()) {
                logger.info("Not generating NewNumber because of generationWindow");
                logger.info("current {} lastNumber {} window {}", currentTimestamp, lastNumberGeneratedAt, game.getGenerationWindow());
                return Optional.empty();
            }
            final int lastNumIdx = game.getNumberIdx();
            final int newNumIdx = game.getNumbers().size()-1;
            logger.info("newNumIdx {}, numbers {}", lastNumIdx, game.getNumbers());
            if (newNumIdx < game.getNumbers().size()) {
                game.setNumberIdx(newNumIdx);
                game.setNumberGeneratedAt(new Timestamp(System.currentTimeMillis()));
                update(game);
            }
            return Optional.of(game);
        }
        return Optional.empty();
    }
}
