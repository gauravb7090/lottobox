package dao;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.slf4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public class AbstractDao<T extends Serializable> {

    private Class<T> clazz;

    private final Provider<EntityManager> entityManagerProvider;

    private final Logger logger;

    @Inject
    public AbstractDao(Provider<EntityManager> entityManagerProvider, Logger logger) {
        this.entityManagerProvider = entityManagerProvider;
        this.logger = logger;
    }

    public void setClazz(Class<T> clazzToSet) {
        this.clazz = clazzToSet;
    }

    public EntityManager getEntityManager() {
        return entityManagerProvider.get();
    }

    public Optional<T> find(Object id) {
        return Optional.ofNullable(getEntityManager().find(clazz, id));
    }

    public List<T> findAll() {
        final EntityManager entityManager = getEntityManager();
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> query = cb.createQuery(clazz);
        query.from(clazz);
        return executeAll(query);
    }

    public List<T> executeAll(CriteriaQuery<T> query) {
        return getEntityManager().createQuery(query).getResultList();
    }

    public Optional<T> execute(CriteriaQuery<T> query) {
        try{
            return Optional.of(getEntityManager().createQuery(query).getSingleResult());
        } catch (NoResultException nre) {
            logger.info("No result found by query {}", query.toString());
            return Optional.empty();
        } catch (NonUniqueResultException nure) {
            logger.error("Non unique result found by query {}", query.toString());
            return Optional.empty();
        }
    }

    @Transactional
    public void save(T entity) {
        getEntityManager().persist(entity);
    }

    @Transactional
    public void update(T entity) {
        getEntityManager().merge(entity);
    }
}
