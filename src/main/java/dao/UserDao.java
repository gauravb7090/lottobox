package dao;

import com.google.inject.Provider;
import models.User;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class UserDao extends AbstractDao<User> {

    public final Logger logger;

    @Inject
    public UserDao(Provider<EntityManager> entityManagerProvider, Logger logger) {
        super(entityManagerProvider, logger);
        setClazz(User.class);
        this.logger = logger;
    }

    public User create(String name) {
        User user = new User();
        user.setName(name);
        save(user);
        return user;
    }
}
