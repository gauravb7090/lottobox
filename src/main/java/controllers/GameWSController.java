package controllers;

import core.WSUtils;
import handlers.GameHandler;
import lombok.SneakyThrows;
import models.dto.GameDto;
import models.dto.request.*;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.websockets.WebSocketHandshake;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.websocket.*;
import java.util.Optional;
import java.util.UUID;

@Singleton
public class GameWSController extends Endpoint implements MessageHandler.Whole<String> {

    private final Logger logger;
    private final GameHandler gameHandler;

    @Inject
    public GameWSController(Logger logger, GameHandler gameHandler) {
        this.logger = logger;
        this.gameHandler = gameHandler;
    }

    public final Result handshake(Context context, WebSocketHandshake handshake) {
        handshake.selectProtocol("game");

        return Results.webSocketContinue();
    }

    @SneakyThrows
    @Override
    public void onOpen(Session session, EndpointConfig config) {
        session.addMessageHandler(this);
        session.setMaxIdleTimeout(0);
        final Optional<Pair<UUID, UUID>> principalOpt = WSUtils.parsePrincipal(session);
        if (principalOpt.isEmpty()) {
            WSUtils.closeParse(session);
            return;
        }
        final Pair<UUID, UUID> principal = principalOpt.get();
        final UUID userId = principal.getLeft();
        final UUID gameId = principal.getRight();
        final Optional<GameDto> resultOpt = addSubscriber(gameId, userId, session);
        if (resultOpt.isEmpty()) {
            WSUtils.closeAuth(session);
        }
    }

    @Override
    public final void onClose(Session session, CloseReason closeReason) {
        WSUtils.parsePrincipal(session).ifPresent(principal -> {
            final UUID userId = principal.getLeft();
            final UUID gameId = principal.getRight();
            removeSubscriber(gameId, userId);
        });
        super.onClose(session, closeReason);
    }

    @Override
    public void onError(Session session, Throwable thr) {
        logger.error(thr.getMessage());
        thr.printStackTrace();
        super.onError(session, thr);
    }

    @Override
    public void onMessage(String requestStr) {
        logger.info("Received request {}", requestStr);
        final WSRequest wsRequest = WSUtils.parseWSRequest(requestStr);
        logger.debug("Received request of type {} with payload {}", wsRequest.requestType, wsRequest.payload);

        final AuthWSRequest wsPayload = WSUtils.parsePayload(wsRequest.requestType, wsRequest.payload);
        WSUtils.validateRequest(wsPayload).ifPresent(parsedReq -> {
            final UUID userId = parsedReq.getLeft();
            final UUID gameId = parsedReq.getRight();
            updateSubscriber(wsRequest.requestType, wsPayload, gameId, userId);
        });
    }

    public Optional<GameDto> addSubscriber(UUID gameId, UUID userId, Session session) {
        return gameHandler.addSubscriber(gameId, userId, session);
    }

    public void removeSubscriber(UUID gameId, UUID userId) {
        gameHandler.removeSubscriber(gameId, userId);
    }

    protected void updateSubscriber(WSRequestType requestType, AuthWSRequest wsPayload, UUID gameId, UUID userId) {
        switch (requestType) {
            case Heartbeat:
                gameHandler.getWSSession(gameId, userId).ifPresent(session -> session.getAsyncRemote().sendText(session.getId()));
                break;
            case GenerateNumber:
                final WSGenerateNumberRequest generateNumberRequest = (WSGenerateNumberRequest) wsPayload;
                gameHandler.generateNumber(gameId, userId);
                break;
            case Claim:
                final WSClaimRequest claimRequest = (WSClaimRequest) wsPayload;
                gameHandler.claim(gameId, userId, claimRequest.type);
                break;
            case NumbersChecked:
                final WSCheckNumberRequest checkNumberRequest = (WSCheckNumberRequest) wsPayload;
                gameHandler.checkNumber(gameId, userId, checkNumberRequest.number, checkNumberRequest.isChecked);
                break;
            case GameStarted:
            case NewUserJoin:
            default:
                gameHandler.getWSSession(gameId, userId).ifPresent(WSUtils::forceClose);
                break;
        }
    }
}
