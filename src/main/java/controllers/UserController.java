package controllers;

import com.google.inject.Inject;
import etc.JsonFilter;
import handlers.UserHandler;
import ninja.FilterWith;
import ninja.Result;

import static controllers.ControllerUtils.successResponse;

@FilterWith(JsonFilter.class)
public class UserController {

    private final UserHandler userHandler;

    @Inject
    public UserController(UserHandler userHandler) {
        this.userHandler = userHandler;
    }

    public Result list() {
        return successResponse(userHandler.list());
    }

}