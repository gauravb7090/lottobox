package controllers;

import com.google.inject.Inject;
import etc.Auth;
import etc.AuthFilter;
import handlers.GameHandler;
import handlers.UserHandler;
import models.dto.GameDto;
import models.dto.UserDto;
import models.dto.request.CreateGameRequest;
import models.dto.request.JoinGameRequest;
import models.dto.request.StartGameRequest;
import models.dto.response.NewGameResult;
import ninja.Context;
import ninja.FilterWith;
import ninja.Result;
import ninja.validation.JSR303Validation;
import ninja.validation.Validation;

import java.util.Optional;
import java.util.UUID;

import static controllers.ControllerUtils.errorResponse;
import static controllers.ControllerUtils.successResponse;

public class GameController {

    private final GameHandler gameHandler;
    private final UserHandler userHandler;

    @Inject
    public GameController(GameHandler gameHandler, UserHandler userHandler) {
        this.gameHandler = gameHandler;
        this.userHandler = userHandler;
    }

    public Result hostGame(@JSR303Validation CreateGameRequest request,
                           Validation validation,
                           Context context) {
        if (validation.hasViolations()) {
            return errorResponse(validation.getViolations().toString());
        }

        final UserDto user = userHandler.getOrCreate(request.name, context);
        final Optional<GameDto> gameV2DtoOpt = gameHandler.create(user, context);

        if (gameV2DtoOpt.isPresent()) {
            final GameDto gameDto = gameV2DtoOpt.get();
            return successResponse(
                    NewGameResult.builder()
                            .gameId(gameDto.getId())
                            .gameCode(gameDto.getCode())
                            .status(gameDto.getStatus())
                            .claimingWindow(gameDto.getClaimingWindow())
                            .reconnectThreshold(gameDto.getReconnectThreshold())
                            .userId(user.getId())
                            .players(gameDto.getMembers())
                            .build()
            );
        }
        return errorResponse("Unable to create game; Please try again");
    }

    public Result joinGame(@JSR303Validation JoinGameRequest request,
                           Validation validation,
                           Context context) {
        if (validation.hasViolations()) {
            return errorResponse(validation.getViolations().toString());
        }

        final UserDto user = userHandler.getOrCreate(request.name, context);
        final Optional<GameDto> gameV2DtoOpt = gameHandler.joinGame(request.gameCode, user, context);

        if (gameV2DtoOpt.isPresent()) {
            final GameDto gameDto = gameV2DtoOpt.get();
            return successResponse(
                    NewGameResult.builder()
                            .gameId(gameDto.getId())
                            .gameCode(gameDto.getCode())
                            .status(gameDto.getStatus())
                            .claimingWindow(gameDto.getClaimingWindow())
                            .reconnectThreshold(gameDto.getReconnectThreshold())
                            .userId(user.getId())
                            .players(gameDto.getMembers())
                            .build()
            );
        }
        return errorResponse("Code not found");
    }

    @FilterWith(AuthFilter.class)
    public Result startGame(@Auth UserDto user,
                            @JSR303Validation StartGameRequest request,
                            Validation validation,
                            Context context) {
        if (validation.hasViolations()) {
            return errorResponse(validation.getViolations().toString());
        }

        final UUID gameId;
        try {
            gameId = UUID.fromString(request.gameId);
        } catch (IllegalArgumentException ex) {
            return errorResponse("Unable to parse Game String");
        }
        final Optional<GameDto> gameV2DtoOpt = gameHandler.startGame(gameId, user);
        if (gameV2DtoOpt.isPresent()) {
            final GameDto gameDto = gameV2DtoOpt.get();
            return successResponse(
                    NewGameResult.builder()
                            .gameId(gameDto.getId())
                            .gameCode(gameDto.getCode())
                            .status(gameDto.getStatus())
                            .claimingWindow(gameDto.getClaimingWindow())
                            .reconnectThreshold(gameDto.getReconnectThreshold())
                            .userId(user.getId())
                            .players(gameDto.getMembers())
                            .build()
            );
        }
        return errorResponse("No such game found");
    }

    public Result list() {
        return successResponse(gameHandler.list());
    }

    @FilterWith(AuthFilter.class)
    public Result generateAllNumbers(@Auth UserDto user,
                                     @JSR303Validation StartGameRequest request,
                                     Validation validation,
                                     Context context) {
        if (validation.hasViolations()) {
            return errorResponse(validation.getViolations().toString());
        }

        final UUID gameId;
        try {
            gameId = UUID.fromString(request.gameId);
        } catch (IllegalArgumentException ex) {
            return errorResponse("Unable to parse Game String");
        }
        final Optional<GameDto> gameV2DtoOpt = gameHandler.generateAllNumbers(gameId, user.getId());
        if (gameV2DtoOpt.isPresent()) {
            final GameDto gameDto = gameV2DtoOpt.get();
            return successResponse(
                    NewGameResult.builder()
                            .gameId(gameDto.getId())
                            .gameCode(gameDto.getCode())
                            .status(gameDto.getStatus())
                            .claimingWindow(gameDto.getClaimingWindow())
                            .reconnectThreshold(gameDto.getReconnectThreshold())
                            .userId(user.getId())
                            .players(gameDto.getMembers())
                            .build()
            );
        }
        return errorResponse("No such game found");
    }
}