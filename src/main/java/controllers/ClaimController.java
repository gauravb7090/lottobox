package controllers;

import com.google.inject.Inject;
import etc.Auth;
import etc.AuthFilter;
import handlers.ClaimHandler;
import models.dto.ClaimDto;
import models.dto.UserDto;
import models.dto.request.ClaimRequest;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.params.PathParam;
import ninja.validation.JSR303Validation;
import ninja.validation.Validation;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class ClaimController {

    private final ClaimHandler handler;

    @Inject
    public ClaimController(ClaimHandler handler) {
        this.handler = handler;
    }

    @FilterWith(AuthFilter.class)
    public Result list(@Auth UserDto user) {
        final List<ClaimDto> claimList = handler.list();
        return Results.json().render(claimList);
    }

    @FilterWith(AuthFilter.class)
    public Result get(@Auth UserDto user,
                      @PathParam("id") String uuid) {
        final UUID id;
        try {
            id = UUID.fromString(uuid);
        } catch (IllegalArgumentException ex) {
            return Results.json().render(Collections.EMPTY_MAP);
        }
        final Optional<ClaimDto> claimDto = handler.get(id);
        if (claimDto.isEmpty()) {
            return Results.json().render(Collections.EMPTY_MAP);
        }
        return Results.json().render(claimDto.get());
    }

    @FilterWith(AuthFilter.class)
    public Result create(@Auth UserDto userDto,
                         @JSR303Validation ClaimRequest request,
                         Validation validation) {
        if (validation.hasViolations()) {
            return Results.json().render(Collections.EMPTY_MAP);
        }

        final UUID ticketId;
        try {
            ticketId = UUID.fromString(request.ticketId);
        } catch (IllegalArgumentException ex) {
            return Results.json().render(Collections.EMPTY_MAP);
        }
        final Optional<ClaimDto> claimDtoOpt = handler.create(userDto.getId(), ticketId, request.type);
        if (claimDtoOpt.isEmpty()) {
            return Results.json().render(Collections.EMPTY_MAP);
        }
        return Results.json().render(claimDtoOpt.get());
    }
}