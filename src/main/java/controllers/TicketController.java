package controllers;

import com.google.inject.Inject;
import etc.Auth;
import etc.AuthFilter;
import handlers.TicketHandler;
import models.dto.TicketDto;
import models.dto.UserDto;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.params.PathParam;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class TicketController {

    private final TicketHandler handler;

    @Inject
    public TicketController(TicketHandler handler) {
        this.handler = handler;
    }

    @FilterWith(AuthFilter.class)
    public Result list(@Auth UserDto user) {
        final List<TicketDto> ticketList = handler.list();
        return Results.json().render(ticketList);
    }

    @FilterWith(AuthFilter.class)
    public Result get(@Auth UserDto user,
                      @PathParam("id") String uuid) {
        final UUID id;
        try {
            id = UUID.fromString(uuid);
        } catch (IllegalArgumentException ex) {
            return Results.json().render(Collections.EMPTY_MAP);
        }
        final Optional<TicketDto> ticketDto = handler.get(id);
        if (ticketDto.isEmpty()) {
            return Results.json().render(Collections.EMPTY_MAP);
        }
        return Results.json().render(ticketDto.get());
    }

    @FilterWith(AuthFilter.class)
    public Result create(@Auth UserDto userDto,
                         @PathParam("gameId") String gameUuid) {
        final UUID gameId;
        try {
            gameId = UUID.fromString(gameUuid);
        } catch (IllegalArgumentException ex) {
            return Results.json().render(Collections.EMPTY_MAP);
        }
        final TicketDto ticketDto = handler.create(userDto, gameId);
        return Results.json().render(ticketDto);
    }
}