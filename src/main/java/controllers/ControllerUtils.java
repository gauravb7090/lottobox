package controllers;

import models.dto.response.Response;
import ninja.Result;
import ninja.Results;

public class ControllerUtils {

    public static Result errorResponse(String errorMessage) {
        return Results.json().render(
                Response.errorBuilder()
                        .error(errorMessage)
                        .build()
        );
    }

    public static <T> Result successResponse(T result) {
        return Results.json().render(
                Response.<T>successBuilder()
                        .result(result)
                        .build()
        );
    }
}
