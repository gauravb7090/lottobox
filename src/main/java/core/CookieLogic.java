package core;

import ninja.Context;
import ninja.session.Session;

import javax.inject.Singleton;
import java.util.UUID;

@Singleton
public class CookieLogic {

    public static void setCookie(String key, UUID id, Context context) {
        final Session session = context.getSession();
        session.put(key, String.valueOf(id));
        session.setExpiryTime(24 * 60 * 60 * 1000L);
    }

    public static void clearCookie(String key, Context context) {
        context.getSession().remove(key);
        context.getSession().clear();
        context.getSession().save(context);
    }
}