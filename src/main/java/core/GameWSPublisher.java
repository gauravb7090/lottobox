package core;

import models.dto.GameDto;
import models.dto.request.WSRequestType;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.UUID;

public class GameWSPublisher extends WSPublisher {

    @Inject
    public GameWSPublisher(UserState userState, TicketState ticketState, Logger logger) {
        super(userState, ticketState, logger);
    }

    @Override
    public void publish(WSRequestType type, GameDto gameDto, UUID caller) {
        getLogger().info("Publishing for requestType {} and gameId {}", type, gameDto.getId());
        switch (type) {
            case NumbersChecked:
                unicast(gameDto, caller);
                return;
            case GenerateNumber:
            case Claim:
            case NewUserJoin:
            case GameStarted:
            default:
                broadcast(gameDto);
        }
    }

}
