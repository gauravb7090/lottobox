package core;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import models.dto.GameDto;
import models.dto.TicketDto;
import models.dto.UserDto;
import models.dto.response.PlayerType;
import models.dto.response.TicketResponse;
import models.dto.response.UserResponse;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.websocket.Session;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Singleton
public class TicketState {

    private final Table<UUID, UUID, Set<Integer>> numbersCheckedMap = HashBasedTable.create();
    private final Logger logger;

    @Inject
    public TicketState(Logger logger) {
        this.logger = logger;
    }

    public Set<Integer> numbersChecked(GameDto gameDto, UUID userId) {
        return Optional.ofNullable(numbersCheckedMap.get(gameDto.getId(), userId))
                .orElse(Collections.EMPTY_SET);
    }

    private Optional<TicketDto> getFirstValidTicket(GameDto gameDto, UUID userId) {
        final Optional<TicketDto> validTicketOpt = gameDto.getTickets()
                .stream()
                .filter(ticket -> ticket.getUserId().equals(userId))
                .findFirst();
        if (validTicketOpt.isEmpty()) {
            logger.error("User {} found with no ticket in game {}", userId, gameDto.getId());
        }
        return validTicketOpt;
    }

    public TicketResponse mapTicket(GameDto gameDto, UUID userId) {
        final Optional<TicketDto> validTicketOpt = getFirstValidTicket(gameDto, userId);
        return validTicketOpt
                .map(validTicket ->
                        TicketResponse.builder()
                                .id(validTicket.getId())
                                .numbers(validTicket.getNumbers())
                                .build())
                .orElse(null);
    }

    public void checkNumber(GameDto gameDto, UUID userId, int number, boolean check) {
        final Optional<TicketDto> validTicketOpt =
                gameDto.getTickets()
                        .stream()
                        .filter(ticketDto -> ticketDto.getUserId().equals(userId))
                        .findFirst();
        validTicketOpt.ifPresent(
                ticket -> {
                    if (ticket.containsNumber(number)) {
                        logger.info("Ticket contains {}", number);
                        if (numbersCheckedMap.contains(gameDto.getId(), userId)) {
                            final Set<Integer> numbersChecked = numbersCheckedMap.get(gameDto.getId(), userId);
                            if (check) {
                                logger.info("Number {} added because checkFlag {}", number, check);
                                numbersChecked.add(number);
                            } else {
                                logger.info("Number {} not added because checkFlag {}", number, check);
                                numbersChecked.remove(number);
                            }
                        } else {
                            if (check) {
                                numbersCheckedMap.put(gameDto.getId(), userId, Sets.newHashSet(number));
                            }
                        }
                    }
                    logger.info("Ticket does not contain {}", number);
                });
    }
}
