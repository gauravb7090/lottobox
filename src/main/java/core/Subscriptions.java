package core;

import com.google.common.collect.Table;
import etc.JsonConverter;
import models.dto.ClaimDto;
import models.dto.GameDto;
import models.dto.request.WSRequestType;
import models.dto.response.ApiResponse;
import models.dto.response.ClaimResponse;
import org.slf4j.Logger;

import javax.websocket.Session;
import java.util.UUID;
import java.util.stream.Collectors;

public interface Subscriptions {

    void addSubscriber(UUID gameId, UUID userId, Session session);

    void removeSubscriber(UUID gameId, UUID userId);

}
