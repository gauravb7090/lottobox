package core;

import com.google.common.collect.Lists;
import models.dto.GameDto;
import models.dto.TicketClaimDto;
import models.dto.VariationsDto;
import ninja.Context;
import ninja.session.Session;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static etc.Constants.GAME_KEY;

@Singleton
public class GameLogic {

    @Inject
    static Logger logger;

    public static List<Integer> GAME_NUMBERS = IntStream.rangeClosed(1, 90).boxed().collect(Collectors.toList());

    public static void setCookie(GameDto gameDto, Context context) {
        final Session session = context.getSession();
        session.put(GAME_KEY, String.valueOf(gameDto.getId()));
        session.setExpiryTime(24 * 60 * 60 * 1000L);
    }

    public static void clearCookie(Context context) {
        context.getSession().remove(GAME_KEY);
        context.getSession().clear();
        context.getSession().save(context);
    }

    public static List<Integer> numbersCalled(GameDto gameDto) {
        final List<Integer> numbersCalled;
        if (gameDto.getNumberIdx() < 0) {
            numbersCalled = Lists.newArrayList();
        } else {
            numbersCalled = gameDto.getNumbers().subList(0, gameDto.getNumberIdx() + 1);
        }
        logger.info("Numbers called {}", numbersCalled);
        return numbersCalled;
    }

    public static boolean claim(VariationType type, TicketClaimDto claimDto) {
        final List<VariationsDto> variations = claimDto.getVariations();
        logger.info("Variations {}", variations);
        final Optional<VariationsDto> variation = claimDto.getVariation(type);
        if (variation.isPresent()) {
            logger.info("Variation {}", variation.get().getNumbers());
        }
        final Optional<Set<Integer>> vNumbersOpt = variation.map(VariationsDto::getNumbers);
        if (vNumbersOpt.isPresent()) {
            logger.info("vNumbers {}", vNumbersOpt.get());
        }
        final List<Integer> numbersCalled = numbersCalled(claimDto.getGame());
        if (numbersCalled.size() <= 0) {
            logger.info("No numbers called");
            return false;
        }
        int lastNumberCalled = numbersCalled.get(numbersCalled.size() - 1);
        logger.info("lastNumberCalled {}", lastNumberCalled);

        return vNumbersOpt
                .map(vNumbers -> {
                    logger.info("vNumbers {}", vNumbers);
                    return numbersCalled.containsAll(vNumbers);
                })
//                        vNumbers.contains(lastNumberCalled) &&
//                                numbersCalled.containsAll(vNumbers))
                .orElse(false);
    }
}