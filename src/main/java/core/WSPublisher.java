package core;

import com.google.common.collect.Table;
import etc.JsonConverter;
import models.dto.ClaimDto;
import models.dto.GameDto;
import models.dto.request.WSRequestType;
import models.dto.response.ApiResponse;
import models.dto.response.ClaimResponse;
import org.slf4j.Logger;

import javax.websocket.Session;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class WSPublisher implements Subscriptions {

    private final UserState userState;
    private final TicketState ticketState;
    private final Logger logger;

    public WSPublisher(UserState userState, TicketState ticketState, Logger logger) {
        this.userState = userState;
        this.ticketState = ticketState;
        this.logger = logger;
    }

    public final ApiResponse createResponse(GameDto gameDto, UUID receiverId) {
        return ApiResponse.builder()
                .gameId(gameDto.getId())
                .gameCode(gameDto.getCode())
                .reconnectThreshold(gameDto.getReconnectThreshold())
                .claimingWindow(gameDto.getClaimingWindow())
                .status(gameDto.getStatus())
                .numbers(GameLogic.numbersCalled(gameDto))
                .players(gameDto.getMembers()
                        .stream()
                        .map(userDto -> userState.mapUser(gameDto, userDto))
                        .collect(Collectors.toList()))
                .claims(gameDto.getClaims()
                        .stream()
                        .map(claimDto -> mapClaim(gameDto, claimDto))
                        .collect(Collectors.toList()))
                .userId(receiverId)
                .numbersChecked(ticketState.numbersChecked(gameDto, receiverId))
                .ticket(ticketState.mapTicket(gameDto, receiverId))
                .build();
    }

    public abstract void publish(WSRequestType type, GameDto gameDto, UUID caller);

    protected void broadcast(GameDto gameDto) {
        dumpSessionMap();
        final UUID gameId = gameDto.getId();
        logger.info("Publishing game {}", gameId);
        if (!getSessionMap().containsRow(gameId)) {
            logger.error("No subscriber to publish for game {}", gameId);
            return;
        }
        logger.info("Map contains gameId {}", gameId);

        gameDto.getMembers()
                .forEach(userDto ->
                        {
                            userState.getSession(gameId, userDto.getId()).ifPresent(session -> {
                                final String responseStr = JsonConverter.to(createResponse(gameDto, userDto.getId()));
                                logger.info("Sending out response {}", responseStr);
                                session.getAsyncRemote().sendText(responseStr);
                            });
                        }
                );
    }

    protected void unicast(GameDto gameDto, UUID userId) {
        dumpSessionMap();
        final UUID gameId = gameDto.getId();
        logger.info("Publishing game {}", gameId);
        if (!getSessionMap().contains(gameId, userId)) {
            logger.error("No subscriber found for gameId:userId {}:{}", gameId,userId);
            return;
        }

        userState.getSession(gameId, userId).ifPresent(session -> {
            final String responseStr = JsonConverter.to(createResponse(gameDto, userId));
            logger.info("Sending out response {}", responseStr);

            session.getAsyncRemote().sendText(responseStr);
        });
    }

    private ClaimResponse mapClaim(GameDto gameDto, ClaimDto claimDto) {
        return ClaimResponse.builder()
                .type(claimDto.getType())
                .claimedBy(userState.mapUser(gameDto, claimDto.getUser()))
                .build();
    }

    private void dumpSessionMap() {
        userState.dumpMap();
    }

    private Table<UUID, UUID, Session> getSessionMap() {
        return userState.getSessionMap();
    }

    public Logger getLogger() {
        return logger;
    }

    @Override
    public void addSubscriber(UUID gameId, UUID userId, Session session) {
        dumpSessionMap();
        userState.addSubscriber(gameId, userId, session);
        dumpSessionMap();
    }

    @Override
    public void removeSubscriber(UUID gameId, UUID userId) {
        dumpSessionMap();
        userState.removeSubscriber(gameId, userId);
        dumpSessionMap();
    }
}
