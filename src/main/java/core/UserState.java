package core;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import models.dto.GameDto;
import models.dto.UserDto;
import models.dto.response.PlayerType;
import models.dto.response.UserResponse;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.websocket.Session;
import java.util.Optional;
import java.util.UUID;

@Singleton
public class UserState implements Subscriptions {

    private final Table<UUID, UUID, Session> sessionMap = HashBasedTable.create();
    private final Logger logger;

    @Inject
    public UserState(Logger logger) {
        this.logger = logger;
    }

    public static PlayerType isHost(GameDto gameDto, UUID userId) {
        return (gameDto.getHost().getId().equals(userId)) ? PlayerType.HOST : PlayerType.PLAYER;
    }

    public boolean isConnected(UUID gameId, UUID userId) {
        return (sessionMap.contains(gameId, userId));
    }

    public UserResponse mapUser(GameDto gameDto, UserDto userDto) {
        return UserResponse.builder()
                .userId(userDto.getId())
                .name(userDto.getName())
                .playerType(isHost(gameDto, userDto.getId()))
                .connectionStatus(isConnected(gameDto.getId(), userDto.getId()))
                .build();
    }

    public Table<UUID, UUID, Session> getSessionMap() {
        return sessionMap;
    }

    public void dumpMap() {
        sessionMap.rowMap().forEach(
                (gameId, userSessionMap) -> userSessionMap.forEach(
                        (userId, session) ->
                                logger.info("gameUserSessionMap [{} {} {}]", gameId, userId, session.getId())
                )
        );
    }

    public Optional<Session> getSession(UUID gameId, UUID userId) {
        return Optional.ofNullable(getSessionMap().get(gameId, userId));
    }

    @Override
    public void addSubscriber(UUID gameId, UUID userId, Session session) {
        sessionMap.put(gameId, userId, session);
    }

    @Override
    public void removeSubscriber(UUID gameId, UUID userId) {
        sessionMap.remove(gameId, userId);
    }

}
