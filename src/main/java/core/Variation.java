package core;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Triple;

import java.util.Collections;
import java.util.List;

import static core.Variation.TRAVERSAL_TYPE.COL;
import static core.Variation.TRAVERSAL_TYPE.ROW;

public enum Variation {

    INVALID(VariationType.INVALID, Collections.EMPTY_LIST),
    COL1(VariationType.FIRST_COL, Lists.newArrayList(Triple.of(COL, 0,3))),
    COL2(VariationType.SECOND_COL, Lists.newArrayList(Triple.of(COL, 1,3))),
    COL3(VariationType.THIRD_COL, Lists.newArrayList(Triple.of(COL, 2, 3))),
    LINE1(VariationType.FIRST_LINE, Lists.newArrayList(Triple.of(ROW, 0,9))),
    LINE2(VariationType.SECOND_LINE, Lists.newArrayList(Triple.of(ROW, 1,9))),
    LINE3(VariationType.THIRD_LINE, Lists.newArrayList(Triple.of(ROW, 2,9))),
    FULL(VariationType.FULL_HOUSE, Lists.newArrayList(Triple.of(ROW, 0,9), Triple.of(ROW, 1,9), Triple.of(ROW, 2,9)));

    public enum TRAVERSAL_TYPE {
        ROW,
        COL
    }

    private final VariationType type;
    private final List<Triple<TRAVERSAL_TYPE, Integer, Integer>> traversal;

    Variation(VariationType type, List<Triple<TRAVERSAL_TYPE, Integer, Integer>> traversal) {
        this.type = type;
        this.traversal = traversal;
    }

    public List<Triple<TRAVERSAL_TYPE, Integer, Integer>> getTraversal() {
        return traversal;
    }

    public static Variation fromType(VariationType type) {
        switch (type) {
            case FIRST_LINE:
                return LINE1;
            case FIRST_COL:
                return COL1;
            case SECOND_LINE:
                return LINE2;
            case SECOND_COL:
                return COL2;
            case THIRD_LINE:
                return LINE3;
            case THIRD_COL:
                return COL3;
            case FULL_HOUSE:
                return FULL;
            case INVALID:
            default:
                return INVALID;
        }
    }
}
