package core;

import etc.JsonConverter;
import models.dto.request.*;
import org.apache.commons.lang3.tuple.Pair;

import javax.websocket.CloseReason;
import javax.websocket.Session;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class WSUtils {

    public static void close(Session session, CloseReason.CloseCodes closeCode, String closeMessage) {
        try {
            session.close(new CloseReason(closeCode, closeMessage));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void closeAuth(Session session) {
        close(session, CloseReason.CloseCodes.UNEXPECTED_CONDITION, "INVALID USER/GAME ID");
    }

    public static void closeParse(Session session) {
        close(session, CloseReason.CloseCodes.UNEXPECTED_CONDITION, "UNABLE TO PARSE USER/GAME ID");
    }

    public static void forceClose(Session session) {
        close(session, CloseReason.CloseCodes.UNEXPECTED_CONDITION, "INVALID REQUEST TYPE");
    }

    public static Optional<Pair<UUID, UUID>> parsePrincipal(Session session) {
        final Map<String, String> pathParameters = session.getPathParameters();
        if (pathParameters.containsKey("userId") && pathParameters.containsKey("gameId")) {
            final String userIdStr = pathParameters.get("userId");
            final String gameIdStr = pathParameters.get("gameId");
            final UUID userId;
            final UUID gameId;
            try {
                userId = UUID.fromString(userIdStr);
                gameId = UUID.fromString(gameIdStr);
                return Optional.of(Pair.of(userId, gameId));
            } catch (IllegalArgumentException ex) {
                return Optional.empty();
            }
        }
        return Optional.empty();
    }

    public static AuthWSRequest parseAuthRequest(String requestStr) {
        return (AuthWSRequest) JsonConverter.from(requestStr, AuthWSRequest.class);
    }

    public static WSRequest parseWSRequest(String requestStr) {
        return (WSRequest) JsonConverter.from(requestStr, WSRequest.class);
    }

    public static AuthWSRequest parsePayload(WSRequestType requestType, String payload) {
        switch (requestType) {
            case GenerateNumber:
            case Heartbeat:
                return parseWSGenerateNumberRequest(payload);
            case Claim:
                return parseWSClaimRequest(payload);
            case NumbersChecked:
                return parseWSCheckNumberRequest(payload);
            default:
                return new AuthWSRequest();
        }

    }

    private static WSClaimRequest parseWSClaimRequest(String payload) {
        return (WSClaimRequest) JsonConverter.from(payload, WSClaimRequest.class);
    }

    private static WSGenerateNumberRequest parseWSGenerateNumberRequest(String payload) {
        return (WSGenerateNumberRequest) JsonConverter.from(payload, WSGenerateNumberRequest.class);
    }

    private static WSCheckNumberRequest parseWSCheckNumberRequest(String payload) {
        return (WSCheckNumberRequest) JsonConverter.from(payload, WSCheckNumberRequest.class);
    }

    public static CheckNumberWSRequest parseCheckNumberRequest(String requestStr) {
        return (CheckNumberWSRequest) JsonConverter.from(requestStr, CheckNumberWSRequest.class);
    }

    public static ClaimWSRequest parseClaimRequest(String requestStr) {
        return (ClaimWSRequest) JsonConverter.from(requestStr, ClaimWSRequest.class);
    }

    public static Optional<UUID> parseUUID(String uuidStr) {
        final UUID uuid;
        try {
            uuid = UUID.fromString(uuidStr);
            return Optional.of(uuid);
        } catch (IllegalArgumentException ex) {
            return Optional.empty();
        }
    }

    public static Optional<Pair<UUID, UUID>> validateRequest(AuthWSRequest request) {
        final Optional<UUID> gameIdOpt = parseUUID(request.gameId);
        final Optional<UUID> userIdOpt = parseUUID(request.userId);
        if (gameIdOpt.isEmpty() || userIdOpt.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(Pair.of(userIdOpt.get(), gameIdOpt.get()));
    }
}
