package core;

public enum VariationType {

    INVALID,
    FIRST_LINE,
    FIRST_COL,
    SECOND_LINE,
    SECOND_COL,
    THIRD_LINE,
    THIRD_COL,
    FULL_HOUSE

}
