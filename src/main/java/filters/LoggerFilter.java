package filters;

import com.google.inject.Inject;
import ninja.Context;
import ninja.Filter;
import ninja.FilterChain;
import ninja.Result;
import org.slf4j.Logger;

/**
 * This is just a demo for a filter. This filter just logs a request in level
 * info. Be inspired and use your own filter.
 * 
 * Filters can be attached to classes or methods via @FilterWith(LoggerFilter.class)
 * 
 * @author ra
 * 
 */
public class LoggerFilter implements Filter {

    private final Logger logger;

    @Inject
    public LoggerFilter(Logger logger) {
        this.logger = logger;

    }

    @Override
    public Result filter(FilterChain chain, Context context) {

        logger.info("Got request from : " + context.getRequestPath());
        return chain.next(context);
    }

}
